import React from "react";
import './Item.css';

function Item(props){
  //agregamos las propiedades de onComplete y onDelete
    return(
        <li className="Item">
            <span 
            className={`Icon Icon-check ${props.completed && 'Icon-check--active'}`} 
            onClick={props.onComplete}> 
        √
      </span>
      <p className={`Item-p ${props.completed && 'Item-p--complete'}`}>
        {props.text}
      </p>
      <span className="Icon Icon-delete" onClick={props.onDelete}>
      
      <img className="imagen" src="https://img.icons8.com/external-wanicon-flat-wanicon/64/000000/external-delete-user-interface-wanicon-flat-wanicon.png"/>
      </span>

        </li>
    );
}

export {Item};