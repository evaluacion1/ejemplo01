import React from "react";
import './Boton.css';
function Boton(props){
    const onClickBoton = (msg) =>{
        alert(msg);
    };

    return(
        <button
         className="Boton"
         onClick={()=> onClickBoton("Aquí se debería abrir el modeal") }
         >
             +
        </button>
    );
}

export {Boton};