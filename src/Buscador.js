import React from "react";
import './Buscador.css';

function Buscador({searchValue, setSearchValue}){
    const onSearchValueChange = (event) => {
        console.log(event.target.value);
        setSearchValue(event.target.value);
    }
    return(
        <input 
        className="Buscador" 
        placeholder="Ingresa...."
        value={searchValue}
        onChange={onSearchValueChange}
        >
            </input>
    )
}

export {Buscador};